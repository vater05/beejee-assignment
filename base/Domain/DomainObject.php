<?php

namespace Base\Domain;

abstract class DomainObject {
    
    protected $id = 0;
    
    final public function getId() {
        return $this->id;
    }
    
    private function filterIdentifier($name) {
        
        return filter_var($name, FILTER_VALIDATE_REGEXP, [
            'options' => [
                'default' => null,
                'regexp' => '/^[a-z_]\w*$/i'
            ]
        ]);
    }
    
    /*
     * TODO: consider private/readonly methods
     */
    final public function __get($name) {
        
        $name = $this->filterIdentifier($name);
        if ($name === null) {
            return null;
        }
        /*
         * email -> getEmail
         * isCompleted -> isCompleted
         */
        $methodName = substr($name, 0, 2) === 'is' ? $name : 'get' . ucfirst($name);
        try {
            // $ref = new \ReflectionClass($this);
            // $method = $ref->getMethod($methodName);
            // return $method->invoke($this);
            
            return $this->$methodName();
            
        } catch (\ReflectionException $e) {
            return null;
        }
    }
    
    /*
     * TODO: consider private/readonly methods
     */
    final public function __set($name, $value) {
        
        $name = $this->filterIdentifier($name);
        if ($name === null) {
            return;
        }
        /*
         * name -> getName
         * isEnabled -> setIsEnabled
         */
        $methodName = 'set' . ucfirst($name);
        try {
            // $ref = new \ReflectionClass($this);
            // $method = $ref->getMethod($methodName);
            // $method->invoke($this, $value);
            $this->$methodName($value);
            
        } catch (\ReflectionException $e) {
            // do nothing
        }
    }
}