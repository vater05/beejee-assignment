<?php

namespace Base\Http;

use Base\Contracts\Http\ISessionManager;

class SessionManager implements ISessionManager {
    
    private $started = false;
    private $session;
    
    public function __construct() {
        session_register_shutdown();
    }
    
    public function start() {
        
        if ($this->started) {
            return $this->session;
        }
        
        if (\PHP_SESSION_ACTIVE === session_status()) {
            throw new \RuntimeException('Failed to start the session: already started by PHP.');
        }
        
        if (headers_sent($file, $line)) {
            throw new \RuntimeException("Failed to start the session because headers have already been sent by \"$file\" at line $line.");
        }
        
        // ok to try and start the session
        if (!session_start()) {
            throw new \RuntimeException('Failed to start the session');
        }
        
        $this->session = new Session($_SESSION);
        $this->session->rotateFlashData();
        $this->started = true;
        
        return $this->session;
    }
    
    public function save() {
        
        if ($this->started) {
            
            $_SESSION = $this->session->data();//array_merge($_SESSION, $this->session->data());
            session_write_close();
            $this->started = false;
        }
    }
    
    public function isStarted() {
        return $this->started;
    }

    public function getSession() {
        return $this->session;
    }
}

?>