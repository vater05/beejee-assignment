<?php

namespace Base\Http\Routing;

use ReflectionClass;
use Base\Contracts\Http\IRequest;
use Base\Exceptions\TooManyRouteNesting;
use Base\Contracts\Http\Routing\IRouter;

class Router implements IRouter {
    
    public $rootNode;
    private $groupNode;
    
    public function __construct() {
        $this->rootNode = new RouteNode('');
    }
    
    public function group($path, $clojure) {
        
        if ($this->groupNode != null) {
            throw new TooManyRouteNesting();
        }
        try {
            $this->groupNode = $this->rootNode->add($path, null, null);
            $clojure();
            
        } finally {
            $this->groupNode = null;
        }
    }
    
    public function resource($basePath, $controller, $op = null) {
        
        $handlers = array(
            'index' => [
                'path' => $basePath,
                'method' => 'GET',
                'handler' => "{$controller}->index"
            ],
            'show' => [
                'path' => $basePath.'/{id}',
                'method' => 'GET',
                'handler' => "{$controller}->show"
            ],
            'create' => [
                'path' => $basePath.'/create',
                'method' => 'GET',
                'handler' => "{$controller}->create"
            ],
            'edit' => [
                'path' => $basePath.'/{id}/edit',
                'method' => 'GET',
                'handler' => "{$controller}->edit"
            ],
            'store' => [
                'path' => $basePath,
                'method' => 'POST',
                'handler' => "{$controller}->store"
            ],
            'update' => [
                'path' => $basePath.'/{id}',
                'method' => 'PUT',
                'handler' => "{$controller}->update"
            ],
            'destroy' => [
                'path' => $basePath.'/{id}',
                'method' => 'DELETE',
                'handler' => "{$controller}->destroy"
            ]
        );
        $actions = array_keys($handlers);
        if (is_array($op)) {
            
            if (array_key_exists('only', $op) && is_array($op['only'])) {
                $actions = $op['only'];
                
            } else if (array_key_exists('except', $op) && is_array($op['except'])) {
                $actions = array_diff($actions, $op['except']);
            }
        }
        
        foreach ($actions as $a) {
            if (array_key_exists($a, $handlers)) {
                $h = $handlers[$a];
                $this->add($h['path'], $h['method'], $h['handler']);
            }
        }
    }
    
    public function add($path, $method, $handler) {
        
        $refParts = explode('->', $handler);
        if (count($refParts) != 2) {
            return;
        }
        /*
         * only add existing handlers
         */
        $methods = get_class_methods($refParts[0]);
        if ($methods != null && in_array($refParts[1], $methods)) {
            
            $node = $this->groupNode != null ? $this->groupNode : $this->rootNode;
            $node->add($path, $method, $handler);
        }
    }
    
    public function getHandler($path, $method, $collectPathVariables = false, &$variables = null) {
        
        $node = $this->rootNode->findChild($path, $collectPathVariables, $variables);
        if ($node != null) {
            return $node->getHandler($method);
        }
    }
    
    public function handleRequest(IRequest $request) {
        
        $variables = [];
        $handler = $this->getHandler($request->path(), $request->method(), true, $variables);
        
        if (empty($handler)) {
            return response()->error(404, $request->wantsJson());
        }
        
        list($class, $method) = explode('->', $handler);
        $controller = di()->get($class);
        
        return di()->call([$controller, $method], $variables);
    }
}

?>