<?php

namespace Base\Http\Routing;

use Base\Utility\Strings;
use Base\Utility\Urls;

class RouteNode {
    
    private $baseName;
    private $variableChild;
    private $children = [];
    private $methods = [];
    
    public function __construct($baseName) {
        $this->baseName = mb_strtolower(trim($baseName));;
    }
    
    private function getOrInsertChild($name) {
        if ($this->hasChild($name)) {
            return $this->getChild($name);
        }
        return $this->children[$name] = new RouteNode($name);
    }
    
    private function getOrCreateVariableChild($name) {
        
        if (! isset($this->variableChild)) {
            $this->variableChild = new RouteNode($name);
        }
        return $this->variableChild;
    }
    
    private function addSplitted($parts, $method, $handler) {
        
        if (count($parts) === 0) {
            if ($method != null && $handler != null) {
                $this->methods[$method] = $handler;
            }
            return $this;
        }
        /*
         * check for a path variable
         */
        $child = null;
        if (preg_match('#^{.+}$#i', $parts[0])) {
            
            $parts[0] = ltrim($parts[0], '{');
            $parts[0] = rtrim($parts[0], '}');
            $child = $this->getOrCreateVariableChild($parts[0]);
            
        } else {
            $child = $this->getOrInsertChild($parts[0]);
        }
        $parts = array_slice($parts, 1);
        return $child->addSplitted($parts, $method, $handler);
    }
    
    public function add($path, $method, $handler) {
        
        if (empty($path) || $path === '/') {
            $this->setHandler($method, $handler);
            return;
        }
        $parts = Urls::splitPath($path);
        return $this->addSplitted($parts, $method, $handler);
    }
    
    private function tryCastValue($value) {
        
        if (is_numeric($value)) {
            if (strpos($value, '.') === FALSE && strpos($value, ',') === FALSE) {
                return intval($value);
            } else {
                return floatval($value);
            }
        } else {
            return $value;
        }
    }
    
    private function findChildSplitted($parts, $collectPathVariables, &$variables) {
        
        if (count($parts) === 0) {
            return $this;
        }
        /*
         * check for a path variable
         */
        $child = $this->getChild($parts[0]);
        if (! isset($child)) {
            
            $child = $this->variableChild;
            if (isset($child)) {
                if ($collectPathVariables) {
                    $variables[$child->baseName] = $this->tryCastValue($parts[0]);
                }
            } else {
                return null;
            }
        }
        $parts = array_slice($parts, 1);
        return $child->findChildSplitted($parts, $collectPathVariables, $variables);
    }
    
    public function findChild($path, $collectPathVariables = false, &$variables = null) {
        
        if (empty($path) || $path === '/') {
            return $this;
        }
        $parts = Urls::splitPath($path);
        return $this->findChildSplitted($parts, $collectPathVariables, $variables);
    }
    
    public function getBaseName() {
        return $this->baseName;
    }
    
    public function isVariable() {
        return $this->isVariable;
    }
    
    public function getHandler($method) {
        return $this->methods[$method];
    }
    
    public function getChildNum() {
        return count($this->children);
    }
    
    public function getChild($name) {
        return $this->children[mb_strtolower(trim($name))];
    }
    
    public function hasChild($name) {
        return array_key_exists(mb_strtolower(trim($name)), $this->children);
    }
    
    public function setHandler($method, $handler) {
        $this->methods[$method] = $handler;
    }
}