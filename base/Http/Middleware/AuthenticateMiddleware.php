<?php

namespace Base\Http\Middleware;

class AuthenticateMiddleware extends MiddlewareAdapter {
    
    public function handle(IRequest $request, $next) {
        return $next($request);
    }
}