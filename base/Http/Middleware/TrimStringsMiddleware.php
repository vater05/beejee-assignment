<?php

namespace Base\Http\Middleware;

use Base\Contracts\Http\IRequest;

class TrimStringsMiddleware extends MiddlewareAdapter {
    
    public function handle(IRequest $request, $next) {

        echo TrimStringsMiddleware::class, '<br>';
        return $next($request);
    }
}