<?php

namespace Base\Http\Middleware;

use Base\Contracts\Http\IMiddleware;
use Base\Contracts\Http\IRequest;

abstract class MiddlewareAdapter implements IMiddleware {

    public function handle(IRequest $request, $next) {
        return $next($request);
    }
    
    public function afterRender(IRequest $request, $response) {}
}