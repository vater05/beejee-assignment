<?php

namespace Base\Http\Middleware;

use Base\Contracts\Http\IRequest;
use Base\Contracts\Http\ISession;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\ISessionManager;

class SessionStartMiddleware extends MiddlewareAdapter {

    /**
     * @Inject
     * @var IApplication
     */
    private $app;

    /**
     * @Inject
     * @var ISessionManager
     */
    private $sessionManager;

    /**
     * @var ISession
     */
    private $session;
    
    public function handle(IRequest $request, $next) {

        $this->session = $this->sessionManager->start();

        echo SessionStartMiddleware::class, '<br>';
        $response = $next($request);

        $this->sessionManager->save();
        return $response;
    }
}