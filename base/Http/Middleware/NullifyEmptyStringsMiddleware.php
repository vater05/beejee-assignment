<?php

namespace Base\Http\Middleware;

use Base\Contracts\Http\IRequest;
use Base\Http\Middleware\MiddlewareAdapter;

class NullifyEmptyStringsMiddleware extends MiddlewareAdapter {

    public function handle(IRequest $request, $next) {

        echo NullifyEmptyStringsMiddleware::class, '<br>';
        return $next($request);
    }
}