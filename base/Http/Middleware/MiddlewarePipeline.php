<?php

namespace Base\Http\Middleware;

class MiddlewarePipeline {

    private $request;
    private $middleware;
    private $last;

    public function send($request) {

        $this->request = $request;
        return $this;
    }

    public function through($middleware) {

        $this->middleware = is_array($middleware) ? $middleware : func_get_args();
        return $this;
    }

    private function prepareDestination($destination) {

        return function ($passable) use ($destination) {
            return $destination($passable);
        };
    }
    
    private function carry() {

        return function($stack, $middleware) {
            return function ($request) use ($stack, $middleware) {
                return $middleware->handle($request, $stack);
            };
        };
    }

    public function then($closure) {

        $stack = array_reduce(
            array_reverse($this->middleware), $this->carry(), $this->prepareDestination($closure)
        );
        return $stack($this->request);
    }
}