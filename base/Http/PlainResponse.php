<?php

namespace Base\Http;

class PlainResponse extends Response {
    
    public function __construct($content = '', $status = 200, $headers = []) {
        parent::__construct($content, $status, $headers);
    }
    
    public function sendContent() {
        echo $this->content;
    }
}

?>