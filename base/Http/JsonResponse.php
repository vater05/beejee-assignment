<?php

namespace Base\Http;

class JsonResponse extends Response {
    
    public function __construct($data = [], $status = 200, $headers = []) {
        
        $headers['Content-Type'] = 'application/json; charset=UTF-8';
        parent::__construct(json_encode($data, JSON_UNESCAPED_UNICODE), $status, $headers);
    }
    
    public function sendContent() {
        echo $this->content;
    }
}

?>