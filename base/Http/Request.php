<?php

namespace Base\Http;

use Base\Contracts\Http\IRequest;
use Base\Utility\Strings;
use Base\Utility\Arrays;

class Request implements IRequest {
    
    private $query;
    private $server;
    private $cookies;
    private $headers;
    
    private $method;
    private $path;
    
    private $content;
    private $contentType;
    private $jsonData;
    private $formData;
    
    public function __construct() {
        $this->initialize();
    }
    
    public function initialize() {
        
        $this->query = $_GET + $_POST;
        $this->server = $_SERVER;
        $this->cookies = $_COOKIE;
        $this->headers = getallheaders();
        
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->path = trim($_GET['path'], '/');
        
        $this->content = file_get_contents('php://input');
        $this->contentType = $_SERVER['CONTENT_TYPE'];
        
        $this->jsonData = '';
        $this->formData = [];
        
        /*
         * hidden input with name="__method"
         */
        if (isset($this->query['__method'])) {
            $this->method = $this->query['__method'];
        }
        
        if (! empty($this->content)) {
            
            if (Strings::containsAny($this->contentType, ['/json', '+json'])) {
                $this->jsonData = json_decode($this->content);
            }
            if (Strings::containsAny($this->contentType, ['/x-www-form-urlencoded', '/form-data'])) {
                parse_str($this->content, $this->formData);
                
                /*
                 * hidden input with name="__method"
                 */
                // if (isset($this->formData['__method'])) {
                //     $this->method = $this->formData['__method'];
                // }
            }
        }
    }
    
    protected function retrieveItem($source, $key, $default) {
        
        if (is_null($key)) {
            return $this->$source;
        }
        $parameter = $this->$source[$key];
        return isset($parameter) ? $parameter : $default;
    }
    
    public function method() {
        return $this->method;
    }
    
    public function path() {
        return $this->path;
    }
    
    public function segments() {
        return preg_split('#/+#i', $this->url);
    }
    
    public function wantsJson() {
        return Strings::containsAny($this->server['HTTP_ACCEPT'], ['/json', '+json']);
    }
    
    public function json() {
        return $this->jsonData;
    }
    
    public function form() {
        return $this->formData;
    }
    
    public function rawContent() {
        return $this->content;
    }
    
    public function exists($key) {
        return array_key_exists($key, $this->query) || array_key_exists($key, $this->formData);
    }
    
    public function has($key) {
        return isset($this->query[$key]) || isset($this->formData[$key]);
    }
    
    public function all() {
        return $this->query + $this->formData;
    }
    
    public function only($keys) {
        $all = $this->all();
        return Arrays::collectNotEmpty($all, $keys);
    }
    
    public function input($key, $default = null) {
        
        if (array_key_exists($key, $this->query)) {
            return $this->query[$key];
            
        } else if (array_key_exists($key, $this->formData)) {
            return $this->formData[$key];
            
        } else {
            return $default;
        }
    }
    
    public function hasHeader($key) {
        return array_key_exists($key, $this->headers);
    }
    
    public function queryString() {
        return http_build_query($this->query());
    }
    
    public function query() {
        
        $query = $this->query;
        unset($query['path']);
        
        return $query;
    }
}

?>