<?php

namespace Base\Http;

use Base\Utility\Files;
use Base\Contracts\Http\IRequest;
use Base\Contracts\Http\IMiddleware;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\Routing\IRouter;
use Base\Contracts\Http\IFrontController;
use Base\Http\Middleware\MiddlewarePipeline;

class FrontController implements IFrontController {

    protected $app;
    protected $router;

    private $middleware = [];

    public function __construct(IApplication $app, IRouter $router) {

        $this->app = $app;
        $this->router = $router;
    }

    private function prepareMiddleware() {

        $classes = array_merge($this->middleware, $this->provideMiddleware());
        return array_map(function ($class) {
            return $this->app->getContainer()->make($class);

        }, $classes);
    }
    
    private function dispatchToRouter() {
        return function ($request) {
            return 'OK';//$this->router->handleRequest($request);
        };
    }

    final public function handleRequest(IRequest $request) {

        $middleware = $this->prepareMiddleware();
        return (new MiddlewarePipeline())
            ->send($request)
            ->through($middleware)
            ->then($this->dispatchToRouter());
        
        // if (! ($response instanceof Response)) {
            
        //     if ($response instanceof View) {
        //         $response = response()->view($response);
                
        //     } else {
        //         $response = response($response);
        //     }
        // }
        // $response->send();
    }

    public function registerMiddleware($middleware) {
        
        if (is_string($middleware) && class_exists($middleware)) {

            $implements = class_implements($middleware);
            if (in_array(IMiddleware::class, $implements)) {

                $this->middleware[] = $middleware;
            }
        }
    }
    
    /**
     * Client can override this to provide custom middleware
     * @return array an array of middleware classes
     */
    public function provideMiddleware() {
        return [];
    }
}