<?php

namespace Base\Http;

use Base\Contracts\Http\IRequest;

class ViewResponse extends Response {
    
    /**
     * @var App\Views\View
     */
    private $view;
    
    public function __construct($view, $status = 200, $headers = []) {
        
        parent::__construct('', $status, $headers);
        $this->view = $view;
    }
    
    public function sendContent() {
        $this->view
            ->def('request', $this->request)
            ->def('user', [
                'role' => session()->get('user.role'),
                'hash' => session()->get('user.hash'),
            ])
            ->render();
    }
}

?>