<?php

namespace Base\Http;

use Base\Contracts\Http\IRequest;

abstract class Response {
    
    /**
     * Status codes translation table.
     *
     * The list of codes is complete according to the
     * {@link http://www.iana.org/assignments/http-status-codes/ Hypertext Transfer Protocol (HTTP) Status Code Registry}
     * (last updated 2016-03-01).
     *
     * Unless otherwise noted, the status code is defined in RFC2616.
     *
     * @var array
     */
    public static $statusTexts = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        421 => 'Misdirected Request',                                         // RFC7540
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        451 => 'Unavailable For Legal Reasons',                               // RFC7725
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates (Experimental)',                      // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    );
    
    public static function translateStatusCode($code) {
        
        $message = static::$statusTexts[$code];
        return isset($message) ? $message : 'Unknown';
    }
    
    /**
     * @Inject
     * @var IRequest
     */
    protected $request;
    
    protected $content;
    protected $statusCode;
    protected $statusText;
    protected $headers;
    protected $cookies = [];
    
    public function __construct($content, $statusCode, $headers) {
        
        $this->setContent($content);
        $this->setStatus($statusCode);
        $this->setHeaders($headers);
    }
    
    /**
     * Sends HTTP headers.
     */
    public function sendHeaders() {
        
        // headers have already been sent by the developer
        if (headers_sent()) {
            return $this;
        }

        // headers
        foreach ($this->headers as $key => $value) {
            header($key.': '.$value, true, $this->statusCode);
        }

        // status
        http_response_code($this->statusCode);

        return $this;
    }
    
    /**
     * 
     */
    public function sendCookies() {
        
        foreach ($this->cookies as $key => $cookie) {
            setcookie("bjass_vater_$key", $cookie['value'], $cookie['expired']);
        }
    }
    
    public function beforeSendHeaders() {}
    
    public function sendContent() {}
    
    public function send() {
        
        $this->beforeSendHeaders();
        $this->sendHeaders();
        $this->sendCookies();
        $this->sendContent();
        
        return $this;
    }
    
    public function getContent() {
        return $this->content;
    }
    
    public function getStatusCode() {
        return $this->statusCode;
    }
    
    public function getHeaders() {
        return $this->headers;
    }
    
    public function setContent($content) {
        $this->content = $content;
    }
    
    public function setStatus($code, $text = null) {
        
        $this->statusCode = $code;
        $this->statusText = $text == null ? static::translateStatusCode($code) : $text;
    }
    
    /**
     * FIXME: Headers must sent before any content render
     */
    protected function setHeaders($headers) {
        $this->headers = $headers;
    }
    
    /**
     * FIXME: Headers must sent before any content render
     */
    protected function setHeader($key, $value) {
        $this->headers[$key] = $value;
    }
    
    public function setCookie($key, $value, $expired = 0) {
        
        if (preg_match('#^\w+$#i', $key) && is_string($value) && is_numeric($expired)) {
            $this->cookies[$key] = [
                'value' => $value,
                'expired' => intval($expired),
            ];
        }
    }
}

?>