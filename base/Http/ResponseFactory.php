<?php

namespace Base\Http;

class ResponseFactory {
    
    public function make($content = '', $status = 200, $headers = []) {
        
        return di()->make('App\Http\PlainResponse', [
            'content' => $content,
            'status' => $status,
            'headers' => $headers,
        ]);
    }
    
    public function json($data = [], $status = 200, $headers = []) {
        
        return di()->make('App\Http\JsonResponse', [
            'data' => $data,
            'status' => $status,
            'headers' => $headers,
        ]);
    }
    
    public function view($view, $status = 200, $headers = []) {
        
        return di()->make('App\Http\ViewResponse', [
            'view' => $view,
            'status' => $status,
            'headers' => $headers,
        ]);
    }
    
    public function redirect($path, $status = 303, $headers = []) {
        
        return di()->make('App\Http\RedirectResponse', [
            'path' => $path,
            'status' => $status,
            'headers' => $headers,
        ]);
    }
    
    public function error($code, $json = false) {
        
        $message = Response::translateStatusCode($code);
        
        if ($json) {
            return $this->json(['error' => ['code' => $code, 'message' => $message]], $code);
            
        } else {
            $view = view('error', ['code' => $code, 'message' => $message]);
            return $this->view($view, $code);
        }
    }
}

?>