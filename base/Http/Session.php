<?php

namespace Base\Http;

use Base\Contracts\Http\ISession;

class Session implements ISession {
    
    /**
     * 
     */
    private $data = [];
    
    public function __construct($data) {
        $this->data = $data;
    }
    
    public function rotateFlashData() {
        
        if (is_array($this->data['__flash.new'])) {
            
            $this->data['__flash.old'] = $this->data['__flash.new'];
            $this->data['__flash.new'] = [];
            
        } else {
            $this->data['__flash.old'] = [];
            $this->data['__flash.new'] = [];
        }
    }
    
    public function exists($key) {
        return array_key_exists($key, $this->data)
            || array_key_exists($key, $this->data['__flash.old']);
    }
    
    public function has($key) {
        return isset($this->data[$key])
            || isset($this->data['__flash.old'][$key]);
    }
    
    public function get($key, $default = null) {
        
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
            
        } elseif (array_key_exists($key, $this->data['__flash.old'])) {
            return $this->data['__flash.old'][$key];
            
        } else {
            if (is_callable($default)) {
                return $default();
            }
            return $default;
        }
    }
    
    public function put($key, $value) {
        $this->data[$key] = $value;
    }
    
    public function push($key, $value) {
        
        $array = $this->data[$key];
        if (! array_key_exists($key) || is_array($array)) {
            $array[] = $value;
        }
    }
    
    public function flash($key, $value) {
        
        if (! is_array($this->data['__flash.new'])) {
            $this->data['__flash.new'] = [];
        }
        $this->data['__flash.new'][$key] = $value;
    }
    
    public function erase($key) {
        unset($this->data[$key]);
    }
    
    public function clear() {
        $this->data = [];
    }
    
    public function data() {
        return $this->data;
    }
}

?>