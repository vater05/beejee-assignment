<?php

namespace Base\Http;

use Base\Contracts\Http\IRequest;
use Base\Validation\Validator;

class RedirectResponse extends Response {
    
    private $path;
    private $queryString;
    
    public function __construct($path, $status = 303, $headers = []) {
        
        parent::__construct(null, $status, $headers);
        $this->path = $path;
    }
    
    public function beforeSendHeaders() {
        
        $path = $this->path;
        if (isset($this->queryString)) {
            $path = $path . '?' . $this->queryString;
        }
        $this->setHeader('Location', $path);
    }
    
    public function sendContent() {
        die();
    }
    
    public function withQuery($query) {
        
        if (is_array($query)) {
            $this->queryString = http_build_query($query);
        }
        return $this;
    }
}

?>