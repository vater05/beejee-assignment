<?php

namespace Base\Providers;

use Base\Http\SessionManager;
use Base\Contracts\Kernel\IProvider;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\ISessionManager;
use Interop\Container\ContainerInterface;

class SessionProvider implements IProvider {
    
    /**
     * will be runned before the application starts
     */
    public function register(IApplication $app) {

        $di = $app->getContainer();
        $di->register(ISessionManager::class, \DI\object(SessionManager::class)->constructor());
    }
    
    /**
     * will be runned after the application has started
     */
    public function bootstrap(IApplication $app) {}
}