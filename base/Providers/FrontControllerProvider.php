<?php

namespace Base\Providers;

use Base\Http\FrontController;
use Base\Contracts\Kernel\IProvider;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\ISessionManager;
use Base\Contracts\Http\IFrontController;
use Base\Contracts\Http\Routing\IRouter;

class FrontControllerProvider implements IProvider {
    
    /**
     * will be runned before the application starts
     */
    public function register(IApplication $app) {

        $di = $app->getContainer();
        $di->register(
            IFrontController::class, \DI\object(FrontController::class)->constructor(
                \DI\get(IApplication::class),
                \DI\get(IRouter::class)
            )
        );
    }
    
    /**
     * will be runned after the application has started
     */
    public function bootstrap(IApplication $app) {}
}