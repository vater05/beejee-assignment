<?php

namespace Base\Providers;

use Base\Http\Request;
use Base\Contracts\Http\IRequest;
use Base\Contracts\Kernel\IProvider;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\ISessionManager;

class HttpRequestProvider implements IProvider {
    
    /**
     * will be runned before the application starts
     */
    public function register(IApplication $app) {

        $di = $app->getContainer();
        
        $di->register(IRequest::class, \DI\object(Request::class)->constructor());
        $di->register('request', \DI\get(IRequest::class));
    }
    
    /**
     * will be runned after the application has started
     */
    public function bootstrap(IApplication $app) {}
}