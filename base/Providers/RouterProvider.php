<?php

namespace Base\Providers;

use Base\Http\Routing\Router;
use Base\Contracts\Kernel\IProvider;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\ISessionManager;
use Base\Contracts\Http\Routing\IRouter;

class RouterProvider implements IProvider {
    
    /**
     * will be runned before the application starts
     */
    public function register(IApplication $app) {

        $di = $app->getContainer();
        
        $di->register(IRouter::class, \DI\object(Router::class)->constructor());
        $di->register('router', \DI\get(IRouter::class));
    }
    
    /**
     * will be runned after the application has started
     */
    public function bootstrap(IApplication $app) {}
}