<?php

namespace Base\Exceptions;

use Exception;

class MethodNotImplementedException extends Exception {}

?>