<?php

namespace Base\Exceptions;

use Exception;

class TypeMismatchException extends Exception {
    
    public function __construct($message) {
        parent::__construct($message);
    }
}

?>