<?php

namespace Base\Exceptions;

use Exception;

class TooManyRouteNesting extends Exception {
    
    public function __construct() {
        parent::__construct('Too many levels of the route nesting');
    }
}

?>