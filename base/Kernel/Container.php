<?php

namespace Base\Kernel;

use DI\ContainerBuilder;
use Base\Contracts\Kernel\IContainer;

class Container implements IContainer {

    private $definitions;
    private $container;
    
    public function build() {
        
        if (! isset($this->container)) {

            $builder = new ContainerBuilder();
            $builder->addDefinitions($this->definitions);
            $builder->useAnnotations(true);
    
            $this->container = $builder->build();
        }
    }

    /**
     * Looks for an entity registered in DI container
     * It should only be used after the application has started.
     */
    public function get($entity) {
        return isset($this->container) ? $this->container->get($entity) : null;
    }
    
    /**
     * 
     */
    public function register($entity, $definition) {

        if (isset($this->container)) {
            $this->container->set($entity, $definition);

        } else {
            $this->definitions[$entity] = $definition;
        }
    }

    /**
     * Creates an object using dependency injection
     */
    public function make($class, array $args = []) {
        return isset($this->container) ? $this->container->make($class, $args) : null;
    }
}