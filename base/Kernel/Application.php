<?php

namespace Base\Kernel;

use Base\Utility\Files;
use Base\Kernel\Container;
use Base\Contracts\Http\IRequest;
use Base\Contracts\Kernel\IProvider;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\Routing\IRouter;
use Base\Contracts\Http\ISessionManager;
use Interop\Container\ContainerInterface;
use Base\Contracts\Http\IFrontController;
use DI\Definition\Helper\DefinitionHelper;

class Application implements IApplication {

    private $rootPath;
    private $config;
    private $container;
    private $providers;

    public function __construct($rootFolder) {

        $this->rootPath = $rootFolder;
        $this->config = require_once Files::join($rootFolder, 'config', 'app.php');
        $this->container = new Container();
    }

    private function buildProviders() {

        $classes = $this->config['providers'];
        
        foreach ($classes as $class) {

            $implements = class_implements($class);
            if (in_array(IProvider::class, $implements)) {

                $provider = new $class();
                $this->providers[] = $provider;
            }
        }
    }

    private function buildContainer() {

        $this->container->register(IApplication::class, $this);
        $this->container->build();
    }

    private function registerMiddleware(IFrontController $controller) {

        $middleware = $this->config['middleware'];
        if (is_array($middleware)) {

            foreach ($middleware as $m) {
                $controller->registerMiddleware($m);
            }
        }
    }

    public function run() {

        $this->buildProviders();

        foreach ($this->providers as $provider) {
            $provider->register($this);
        }

        $this->buildContainer();

        foreach ($this->providers as $provider) {
            $provider->bootstrap($this);
        }

        $request = $this->container->get(IRequest::class);
        $controller = $this->container->get(IFrontController::class);

        $this->registerMiddleware($controller);
        $response = $controller->handleRequest($request);

        var_dump($response);
    }

    /**
     * Project directory full name
     */
    public function rootPath() {
        return $this->rootPath;
    }

    /**
     * Interface to internal DI container
     * @return Base\Contracts\IContainer
     */
    public function getContainer() {
        return $this->container;
    }
}