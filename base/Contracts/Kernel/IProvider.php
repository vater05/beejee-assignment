<?php

namespace Base\Contracts\Kernel;

use Base\Contracts\Kernel\IApplication;

interface IProvider {

    /**
     * will be runned before the application starts
     */
    public function register(IApplication $app);

    /**
     * will be runned after the application has started
     */
    public function bootstrap(IApplication $app);
}