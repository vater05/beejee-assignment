<?php

namespace Base\Contracts\Kernel;

interface IApplication {

    /**
     * Project directory full name
     */
    public function rootPath();

    /**
     * Interface to internal DI container
     * @return Base\Contracts\IContainer
     */
    public function getContainer();
}