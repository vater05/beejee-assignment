<?php

namespace Base\Contracts\Kernel;

interface IContainer {

    /**
     * Looks for an entity registered in DI container
     * It should only be used after the application has started.
     */
    public function get($entity);

    /**
     * 
     */
    public function register($entity, $definition);

    /**
     * Creates an object using dependency injection
     */
    public function make($class, array $args = []);
}