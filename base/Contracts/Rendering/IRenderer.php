<?php

namespace Base\Contracts\Rendering;

interface IRenderer {
    public function render($view, $args = []);
}