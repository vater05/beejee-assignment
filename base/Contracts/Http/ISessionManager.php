<?php

namespace Base\Contracts\Http;

interface ISessionManager {

    /**
     * @return ISession
     */
    public function start();

    /**
     * 
     */
    public function save();

    /**
     * @return boolean
     */
    public function isStarted();

    /**
     * @return ISession
     */
    public function getSession();
}