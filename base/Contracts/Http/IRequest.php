<?php

namespace Base\Contracts\Http;

interface IRequest {
    
    /**
     * Get the request method.
     */
    public function method();
    
    /**
     * 
     */
    public function path();
    
    /**
     * 
     */
    public function segments();
    
    /**
     * Determine if the current request is asking for JSON in return.
     */
    public function wantsJson();
    
    /**
     * 
     */
    public function json();
    
    /**
     * 
     */
    public function form();
    
    /**
     * 
     */
    public function rawContent();
    
    /**
     * Determine if the request contains a given input item key.
     */
    public function exists($key);
    
    /**
     * Determine if the request contains a non-empty value for an input item.
     */
    public function has($key);
    
    /**
     *
     */
    public function all();
    
    /**
     *
     */
    public function only($keys);
    
    /**
     * 
     */
    public function input($key, $default = null);
    
    /**
     * 
     */
    public function hasHeader($key);
    
    /**
     * 
     */
    public function queryString();
    
    /**
     * 
     */
    public function query();
}

?>