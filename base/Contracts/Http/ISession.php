<?php

namespace Base\Contracts\Http;

interface ISession {

    public function exists($key);
    public function has($key);
    public function get($key, $default = null);
    public function put($key, $value);
    public function push($key, $value);
    public function flash($key, $value);
    public function erase($key);
    public function clear();
    public function data();
}

