<?php

namespace Base\Contracts\Http\Routing;

use Base\Contracts\Http\IRequest;

interface IRouter {

    public function group($path, $clojure);
    public function resource($basePath, $controller, $op = null);
    public function add($path, $method, $handler);
    public function handleRequest(IRequest $request);
}