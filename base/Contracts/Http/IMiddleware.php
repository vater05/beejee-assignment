<?php

namespace Base\Contracts\Http;

interface IMiddleware {

    /**
     * @param $request App\Contracts\Http\IRequest
     * @param $next callable
     */
    public function handle(IRequest $request, $next);

    /**
     * @param $request App\Contracts\Http\IRequest
     * @param $response
     */
    public function afterRender(IRequest $request, $response);
}