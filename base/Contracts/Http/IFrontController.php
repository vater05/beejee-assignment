<?php

namespace Base\Contracts\Http;

use Base\Contracts\Http\IRequest;

interface IFrontController {

    /**
     * @return Response
     */
    public function handleRequest(IRequest $request);

    /**
     * 
     */
    public function registerMiddleware($middleware);
}