<?php

namespace Base\Utility;

class Strings {
    
    private static function any($str, $what, $predicate) {
        
        if (! is_string($str)) {
            return false;
        }
        if (is_string($what)) {
            $what = [$what];
        }
        if (is_array($what)) {
            foreach ($what as $w) {
                if ($predicate($str, $w)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static function containsAny($str, $what) {
        return static::any($str, $what, function ($s, $w) {
            return strpos($s, $w) !== false;
        });
    }
    
    public static function equalsAny($str, $what) {
        return static::any($str, $what, function ($s, $w) {
            return $s === $w;
        });
    }
    
    public static function startsWith($str, $prefix) {
        return is_string($str)
            && is_string($prefix)
            && preg_match("/^${prefix}/i", $str);
    }
    
    public static function endsWith($str, $suffix) {
        return is_string($str)
            && is_string($suffix)
            && preg_match("/${suffix}$/i", $str);
    }
    
    public static function matchIdentifier($str) {
        return preg_match('#^[a-z_]\w*$#i', $str);
    }
    
    public static function preg_ltrim($str, $pattern = '\s+') {
        return preg_replace("%^$pattern%", '', $str);
    }
    
    public static function preg_rtrim($str, $pattern = '\s+') {
        return preg_replace("%$pattern$%", '', $str);
    }
    
    public static function preg_trim($str, $pattern = '\s+') {
        return self::preg_rtrim(self::preg_ltrim($str, $pattern), $pattern);
    }
}

?>