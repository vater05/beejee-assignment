<?php

namespace Base\Utility;

class Files {
    
    /**
     * Extract the file name from a file path
     */
    public function name($path) {
        return pathinfo($path, PATHINFO_FILENAME);
    }

    /**
     * Extract the trailing name component from a file path
     */
    public function basename($path) {
        return pathinfo($path, PATHINFO_BASENAME);
    }

    /**
     * Extract the parent directory from a file path
     */
    public function dirname($path) {
        return pathinfo($path, PATHINFO_DIRNAME);
    }

    /**
     * Extract the file extension from a file path
     */
    public static function extension($path) {
        return pathinfo($path, PATHINFO_EXTENSION);
    }
    
    public static function join() {
        
        $args = func_get_args();
        $paths = array();
        
        if (count($args) > 0) {
            
            if (is_array($args[0])) {
                $paths = $args[0];
                
            } else {
                foreach ($args as $arg) {
                    if (! empty($arg)) {
                        $paths[] = $arg;
                    }
                }
            }
        }
        
        $ret = join(DIRECTORY_SEPARATOR, $paths);
        return preg_replace('#' . DIRECTORY_SEPARATOR . '+#i', DIRECTORY_SEPARATOR, $ret);
    }
    
    public static function randomFileName($dir, $suffix = '', $prefix = '') {
        
        while (true) {
            $filename = uniqid($prefix, true) . $suffix;
            if (!file_exists(static::join($dir, $filename))) break;
        }
        return $filename;
    }
}