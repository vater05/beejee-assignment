<?php

namespace Base\Utility;

use Base\Exceptions\TypeMismatchException;

class Urls {
    
    public static function splitPath($path) {
        
        if (! is_string($path)) {
            throw new TypeMismatchException('Urls::splitPath expects parameter 1 to be string');
        }
        
        $path = preg_replace('#^[\s\\/]+#', '', $path); /* trim left */
        $path = preg_replace('#[\s\\/]+$#', '', $path); /* trim right */
        
        return preg_split('#/+#i', $path);
    }
}

?>