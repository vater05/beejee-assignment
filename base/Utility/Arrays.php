<?php

namespace Base\Utility;

class Arrays {
    
    private static function emptyKey($a, $k) {
        return empty($a[$k]);
    }
    
    private static function notEmptyKey($a, $k) {
        return !empty($a[$k]);
    }
    
    private static function issetKey($a, $k) {
        return isset($a[$k]);
    }
    
    public static function issetAll($array, $keys) {
        
        foreach ($keys as $key) {
            if (! isset($array[$key])) {
                return false;
            }
        }
        return true;
    }
    
    public static function notEmptyAll($array, $keys) {
        
        foreach ($keys as $key) {
            if (empty($array[$key])) {
                return false;
            }
        }
        return true;
    }
    
    private static function collect($array, $keys, $op) {
        
        if (is_array($array) && is_array($keys)) {
            $result = [];
            foreach ($keys as $k) {
                if (static::$op($array, $k)) {
                    $result[$k] = $array[$k];
                }
            }
            return $result;
        }
        return [];
    }
    
    public static function collectNotEmpty($array, $keys) {
        return static::collect($array, $keys, 'notEmptyKey');
    }
    
    public static function collectIsset($array, $keys) {
        return static::collect($array, $keys, 'issetKey');
    }
}

?>