<?php

namespace Base\Validation;

class Validator {
    
    private $input;
    private $entries = [];
    private $messages = [];
    
    public function number($name) {
        
        $this->entries[$name] = new NumberValidator($name);
        return $this->entries[$name];
    }
    
    public function string($name) {
        
        $this->entries[$name] = new StringValidator($name);
        return $this->entries[$name];
    }
    
    public function boolean($name) {
        
        $this->entries[$name] = new BooleanValidator($name);
        return $this->entries[$name];
    }
    
    public function email($name) {
        
        $this->entries[$name] = new EmailValidator($name);
        return $this->entries[$name];
    }
    
    public function passes($input) {
        
        $this->input = $input;
        $passes = true;
        
        foreach ($this->entries as $name => $validator) {
            if ($validator->fails($input[$name])) {
                $passes = false;
                $this->messages[$name] = $validator->message();
            }
        }
        return $passes;
    }
    
    public function fails($input) {
        return !$this->passes($input);
    }
    
    public function input() {
        return $this->input;
    }
    
    public function messages() {
        return $this->messages;
    }
}

?>