<?php

namespace Base\Validation;

class StringValidator extends ValueValidator {
    
    /**
     * 
     */
    protected $pattern;
    protected $maxLength;
    
    public function __construct($name) {
        parent::__construct($name);
    }
    
    public function match($pattern) {
        $this->pattern = $pattern;
    }
    
    public function maxLength($length) {
        $this->maxLength = $length;
        return $this;
    }
    
    public function passes($value) {
        
        if (isset($value)) {
            
            /*
             * check for string
             */
            $passes = is_string($value);
            if (! $passes) {
                $this->message = "\"{$this->name}\" should be string";
                return false;
            }
            
            /*
             * check not empty
             */
            $passes = !empty($value);
            if (! $passes) {
                $this->message = "\"{$this->name}\" should not be empty";
                return false;
            }
            
            /*
             * check maximum length
             */
            if (isset($this->maxLength)) {
                $passes = strlen($value) <= $this->maxLength;
                if (! $passes) {
                    $this->message = "The length of \"{$this->name}\" should be max {$this->maxLength}";
                    return false;
                }
            }
            
            /*
             * check for pattern matching
             */
            if (isset($this->pattern)) {
                $passes = boolval(preg_match($this->pattern, $value));
                
                if (! $passes) {
                    $this->message = "\"{$this->name}\" should match following pattern {$this->pattern}";
                    return false;
                }
            }
            
            /*
             * everything is ok
             */
            return true;
            
        } else {
            return parent::passes($value);
        }
    }
}

?>