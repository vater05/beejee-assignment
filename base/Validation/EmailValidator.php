<?php

namespace Base\Validation;

class EmailValidator extends ValueValidator {
    
    public function __construct($name) {
        parent::__construct($name);
    }
    
    public function passes($value) {
        
        if (isset($value)) {
            $passes = filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
            if (! $passes) {
                $this->message = "\"{$this->name}\" should be valid e-mail address";
            }
            return $passes;
            
        } else {
            return parent::passes($value);
        }
    }
}

?>