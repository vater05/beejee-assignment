<?php

namespace Base\Validation;

abstract class ValueValidator {
    
    /**
     * 
     */
    protected $name;
    protected $required = false;
    protected $message;
    
    public function __construct($name) {
        $this->name = $name;
    }
    
    public function required() {
        $this->required = true;
        return $this;
    }
    
    public function passes($value) {
        $passes = !($this->required && !isset($value));
        if (! $passes) {
            $this->message = "\"{$this->name}\" is required";
        }
        return $passes;
    }
    
    public function fails($value) {
        return !$this->passes($value);
    }
    
    public function name() {
        return $this->name;
    }
    
    public function message() {
        return $this->message;
    }
}

?>