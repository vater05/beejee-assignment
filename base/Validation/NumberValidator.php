<?php

namespace Base\Validation;

class NumberValidator extends ValueValidator {
    
    public function __construct($name) {
        parent::__construct($name);
    }
    
    public function passes($value) {
        
        if (isset($value)) {
            $passes = is_numeric($value);
            if (! $passes) {
                $this->message = "\"{$this->name}\" should be number";
            }
            return $passes;
            
        } else {
            return parent::passes($value);
        }
    }
}

?>