<?php

namespace Base\Validation;

class BooleanValidator extends ValueValidator {
    
    public function __construct($name) {
        parent::__construct($name);
    }
    
    public function passes($value) {
        
        if (isset($value)) {
            $passes = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null;
            if (! $passes) {
                $this->message = "\"{$this->name}\" should be boolean";
            }
            return $passes;
            
        } else {
            return parent::passes($value);
        }
    }
}

?>