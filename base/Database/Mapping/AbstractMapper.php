<?php

namespace Base\Database\Mapping;

use PDO;
use PDOException;

abstract class AbstractMapper {
    
    protected $loadedObjects = [];
    
    abstract protected function findStatement();
    abstract protected function doLoad(array $data);
    
    final protected function abstractFind($id) {
        
        if (array_key_exists($id, $this->loadedObjects)) {
            return $this->loadedObjects[$id];
            
        } else try {
            $stmt = db()->prepare($this->findStatement());
            $stmt->execute([':id' => $id]);
            return $this->load($stmt->fetch());
            
        } catch (PDOException $e) {
            return null;
        }
    }
    
    /*
     * @param $source StatementSource
     */
    final protected function abstractFindMany($source) {
        
        try {
            $stmt = db()->prepare($source->statement());
            $stmt->execute($source->parameters());
            return $this->loadAll($stmt->fetchAll());
            
        } catch (PDOException $e) {
            return null;
        }
    }
    
    final protected function load(array $data) {
        
        $id = intval($data['id']);
        
        if (array_key_exists($id, $this->loadedObjects)) {
            return $this->loadedObjects[$id];
            
        } else {
            $object = $this->doLoad($data);
            $this->loadedObjects[$id] = $object;
            return $object;
        }
    }
    
    final protected function loadAll(array $rawList) {
        
        $objectList = [];
        foreach ($rawList as $data) {
            $objectList[] = $this->load($data);
        }
        return $objectList;
    }
}