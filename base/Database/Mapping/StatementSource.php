<?php

namespace Base\Contracts\Mapping\Database;

class StatementSource {
    
    private $statement;
    private $parameters;
    
    public function __construct($statement, $parameters = []) {
        $this->statement = $statement;
        $this->parameters = $parameters;
    }
    
    /**
     * @return string a query string
     */
    public function statement() {
        return $this->statement;
    }
    
    /**
     * @return array an array of parameters as :name => value
     */
    public function parameters() {
        return $this->parameters;
    }
}