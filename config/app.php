<?php

return [
    'providers' => [
        \Base\Providers\RouterProvider::class,
        \Base\Providers\HttpRequestProvider::class,
        \Base\Providers\SessionProvider::class,
        \Base\Providers\FrontControllerProvider::class,

        \App\Providers\DataMapperProvider::class,
        \App\Providers\RoutesProvider::class,
    ],

    'middleware' => [
        \Base\Http\Middleware\SessionStartMiddleware::class,
        \Base\Http\Middleware\TrimStringsMiddleware::class,
        \Base\Http\Middleware\NullifyEmptyStringsMiddleware::class,

        \App\Http\Middleware\HandleUploadingMiddleware::class,
    ],
];