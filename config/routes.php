<?php

router()->add('/', 'GET', 'App\Http\Controllers\IndexController->index');
router()->add('home', 'GET', 'App\Http\Controllers\IndexController->home');
router()->add('test', 'GET', 'App\Http\Controllers\IndexController->test');
router()->add('test2', 'GET', 'App\Http\Controllers\IndexController->test2');

router()->resource('tasks', 'App\Http\Controllers\TaskController');
router()->resource('api/tasks', 'App\Http\Controllers\Api\TaskApiController', ['only' => ['index', 'show']]);

router()->add('login', 'GET', 'App\Http\Controllers\AuthController->getLogin');
router()->add('login', 'POST', 'App\Http\Controllers\AuthController->postLogin');
router()->add('logout', 'GET', 'App\Http\Controllers\AuthController->logout');
router()->add('session', 'GET', 'App\Http\Controllers\AuthController->dumpSession');

?>