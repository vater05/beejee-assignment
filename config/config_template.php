<?php

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'mvc');
define('DB_USER', 'vater');
define('DB_PASS', '');
define('DB_CHARSET', 'utf8');

define('TWIG_DEBUG', false);
define('TWIG_AUTO_RELOAD', true);
define('TWIG_USE_CACHE', true);