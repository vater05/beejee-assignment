<?php

use Phinx\Seed\AbstractSeed;

class TasksSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        // addColumn('title', 'string')
        // addColumn('body', 'text')
        // addColumn('user_name', 'string')
        // addColumn('user_email', 'string')
        // addColumn('completed', 'boolean')
              
        $data = [
            [
                'title'         => 'Новая задача',
                'body'          => 'Текст задачи длинный длинный.',
                'user_name'     => 'Yan',
                'user_email'    => 'yan@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Ещё новая задача',
                'body'          => 'Текст задачи ещё более длинный.',
                'user_name'     => 'Max',
                'user_email'    => 'max@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Новая задача',
                'body'          => 'Текст задачи длинный длинный.',
                'user_name'     => 'Art',
                'user_email'    => 'art@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Ещё новая задача',
                'body'          => 'Текст задачи ещё более длинный.',
                'user_name'     => 'Vit',
                'user_email'    => 'vit@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Новая задача',
                'body'          => 'Текст задачи длинный длинный.',
                'user_name'     => 'Ira',
                'user_email'    => 'ira@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Ещё новая задача',
                'body'          => 'Текст задачи ещё более длинный.',
                'user_name'     => 'Gal',
                'user_email'    => 'gal@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Новая задача',
                'body'          => 'Текст задачи длинный длинный.',
                'user_name'     => 'Alx',
                'user_email'    => 'alx@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Ещё новая задача',
                'body'          => 'Текст задачи ещё более длинный.',
                'user_name'     => 'Ola',
                'user_email'    => 'ola@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Новая задача',
                'body'          => 'Текст задачи длинный длинный.',
                'user_name'     => 'Yan',
                'user_email'    => 'yan@fil.ru',
                'completed'     => false,
            ],
            [
                'title'         => 'Ещё новая задача',
                'body'          => 'Текст задачи ещё более длинный.',
                'user_name'     => 'Max',
                'user_email'    => 'max@fil.ru',
                'completed'     => false,
            ],
        ];

        $posts = $this->table('tasks');
        $posts->insert($data)
              ->save();
    }
}
