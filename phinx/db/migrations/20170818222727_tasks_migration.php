<?php

use Phinx\Migration\AbstractMigration;

class TasksMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
     
    // public function change()
    // {
    //     // create the table
    //     $table = $this->table('user_logins');
    //     $table->addColumn('user_id', 'integer')
    //           ->addColumn('created', 'datetime')
    //           ->create();
    // }

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tasks = $this->table('tasks');
        $tasks->addColumn('title', 'string')
              ->addColumn('body', 'text')
              ->addColumn('user_name', 'string')
              ->addColumn('user_email', 'string')
              ->addColumn('completed', 'boolean')
              ->addTimestamps()
              ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
