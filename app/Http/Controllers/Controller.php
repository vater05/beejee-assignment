<?php

namespace App\Http\Controllers;

use Base\Exceptions\BadMethodCallException;

abstract class Controller {
    
    /**
     * Execute an action on the controller.
     */
    public function callAction($method, $parameters) {
        return call_user_func_array([$this, $method], $parameters);
    }
    
    /**
     * Handle calls to missing methods on the controller.
     * @throws App\Exceptions\BadMethodCallException
     */
    public function __call($method, $parameters) {
        throw new BadMethodCallException("Method [$method] does not exist.");
    }
}

?>