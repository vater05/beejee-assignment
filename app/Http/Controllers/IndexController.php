<?php

namespace App\Http\Controllers;

use Base\Rendering\View;
use Base\Contracts\Http\IRequest;

class IndexController extends Controller {
    
    /**
     * Controls number of tasks that are displayed on the main page
     */
    private $tasksPerPage = 3;
    
    public function index(IRequest $request) {
        
        $q = Query::make('tasks');
        $pageCount = $q->pageCount($this->tasksPerPage);
        
        if ($pageCount === false) {
            return response()->error(500);
        }
        $page = 1;
        $orderBy = 'id';
        $order = 'asc';
        
        if ($request->has('page') && is_numeric($request->input('page'))) {
            $page = intval($request->input('page'));
        }
        $q->page($page, $this->tasksPerPage);
        
        if ($request->has('order_by')) {
            $orderBy = $request->input('order_by');
            $order = 'asc';
            
            if ($request->has('order')) {
                $order = $request->input('order');
            }
            $q->orderBy($orderBy, $order);
        }
        $tasks = $q->getAll();
        return view('index', [
            'tasks' => $tasks,
            'page' => $page,
            'page_count' => $pageCount,
            'order_by' => $orderBy,
            'order' => $order,
        ]);
    }
    
    public function home(IRequest $request) {
        return view('home', ['request' => $request]);
    }
    
    public function test(IRequest $request) {
        return view('home', ['request' => $request]);
    }
    
    public function test2(IRequest $request) {
        return view('home', ['request' => $request]);
    }
}

?>