<?php

namespace App\Http\Controllers;

use Base\Contracts\Http\IRequest;

class AuthController extends Controller {
    
    public function getLogin(IRequest $request) {
        
        $input = session()->get('__input');
        $errors = session()->get('__errors');
        
        return view('login', [
            '__input' => $input,
            '__errors' => $errors,
            'redirect_to' => urlencode($request->input('redirect_to')),
        ]);
    }
    
    public function postLogin(IRequest $request) {
        
        /*
         * FIXME: hard coding the authentication process
         */
        if ($request->input('password') === '123') {
            
            session()->put('user.role', 'admin');
            session()->put('user.hash', base_convert(rand(0xff, 0xffffffff), 10, 32));
            
            $redirectTo = '/';
            
            if ($request->has('redirect_to')) {
                $redirectTo = $request->input('redirect_to');
            }
            // var_dump($request->input('redirect_to'));
            return response()->redirect($redirectTo);
            
        } else {
            
            session()->flash('__input', $request->all());
            session()->flash('__errors', ['password' => 'wrong password']);
            
            $response = response()->redirect('/login');
            if ($request->has('redirect_to')) {
                $response->withQuery(['redirect_to' => $request->input('redirect_to')]);
            }
            return $response;
        }
    }
    
    public function logout(IRequest $request) {
        
        $requestHash = $request->input('hash');
        $sessionHash = session()->get('user.hash');
        
        if (! empty($requestHash) && $requestHash === $sessionHash) {
            session()->clear();
            return response()->redirect('/');
            
        } else {
            return response()->error(403, true);
        }
    }
    
    public function dumpSession() {
        var_dump(session()->data());
    }
}

?>