<?php

namespace App\Http\Controllers\Api;

use Base\Rendering\View;
use Base\Contracts\Http\IRequest;
use Base\Contracts\Database\Repositories\ITaskRepository;
use App\Http\Controllers\Controller;

class TaskApiController extends Controller {
    
    /**
     * @Inject
     * @var ITaskRepository
     */
    private $taskRepo;
    
    /**
     * GET /api/tasks
     */
    public function index(IRequest $request) {
        $tasks = $this->taskRepo->getAll();
        return response()->json(['data' => $tasks]);
    }
    
    /**
     * GET /api/tasks/{id}
     */
    public function show(IRequest $request, $id) {
        $task = $this->taskRepo->get($id);
        return response()->json(['data' => $task]);
    }
    
    /**
     * POST /api/tasks
     */
    public function store() {
        
    }
    
    /**
     * PUT /api/tasks/{id}
     */
    public function update() {
        
    }
    
    /**
     * DELETE /api/tasks/{id}
     */
    public function destroy() {
        
    }
}

?>