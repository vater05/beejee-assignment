<?php

namespace App\Http\Controllers;

use Base\Rendering\View;
use Base\Contracts\Http\IRequest;
use Base\Utility\Arrays;
use Base\Utility\Paths;
use Base\Validation\Validator;

class TaskController extends Controller {
    
    /**
     * GET /tasks
     */
    public function index(IRequest $request) {
        
        // $q = Query::make('tasks');
        $pageCount = 10;//$q->pageCount($this->tasksPerPage);
        
        if ($pageCount === false) {
            return response()->error(500);
        }
        $page = 1;
        
        if ($request->has('page') && is_numeric($request->input('page'))) {
            $page = intval($request->input('page'));
        }
        return view('task-list', [
            'page' => $page,
            'page_count' => $pageCount
        ]);
    }
    
    /**
     * GET /tasks/{id}
     */
    public function show(IRequest $request, $id) {
        
        $task = $this->taskRepo->get($id);
        if (empty($task)) {
            return response()->error(404);
        } else {
            return view('task-show', ['task' => $task]);
        }
    }
    
    /**
     * GET /tasks/create
     */
    public function create(IRequest $request) {
        
        $input = session()->get('__input');
        $errors = session()->get('__errors');
        
        return view('task-edit', [
            '__input' => $input,
            '__errors' => $errors,
        ]);
    }
    
    /**
     * GET /tasks/{id}/edit
     */
    public function edit($id) {
        
        /*
         * FIXME: hard-coding of the authentication process
         */
        if (session()->get('user.role') !== 'admin') {
            return response()->redirect('/login')->withQuery(['redirect_to' => "/tasks/$id/edit"]);
        }
        
        $task = $this->taskRepo->get($id);
        return view('task-edit', ['task' => $task]);
    }
    
    /*
     * FIXME: this function has too much of responsibility!!!
     */
    private function transformImage($transform) {
        
        $uplName = app()->keepUploadedFile('__image');
        $ext = Paths::ext($uplName);
        
        if ($uplName !== null) {
            
            $tx = intval($transform[0]);
            $ty = intval($transform[1]);
            $rw = intval($transform[2]);
            $rh = intval($transform[3]);
            
            $uplPath = Paths::join(app()->uploadPath(), $uplName);
            $cropImage = image_manager()->make($uplPath)->resize($rw, $rh)->crop(320, 240, $tx, $ty);
            
            $cropImageName = Paths::randomFileName(app()->uploadPath(), ".$ext", 'cropit-');
            $cropImagePath = Paths::join(app()->uploadPath(), $cropImageName);
            $cropImage->save($cropImagePath);
            
            return app()->uploadUrl($cropImageName);
        }
        return null;
    }
    
    /**
     * POST /tasks
     */
    public function store(IRequest $request) {
        
        $task = $request->only([
            'title',
            'user_name',
            'user_email',
            'body',
        ]);
        
        $imagePath = $this->transformImage($request->input('__transform'));
        if ($imagePath === null) {
            return response()->error(500);
        }
        $task['image'] = $imagePath;
        
        $validator = new Validator();
        $validator->string('title')->required();
        $validator->string('body')->required();
        $validator->string('user_name')->required();
        $validator->email('user_email')->required();
        
        if ($validator->fails($task)) {
            
            // var_dump($input);
            // var_dump($validator->messages());
            
            session()->flash('__input', $request->all());
            session()->flash('__errors', $validator->messages());
            
            return response()->redirect('/tasks/create');
            
        } else {
            
            $q = Query::make('tasks');
            $count = $q->save($task);
            
            if ($count === 1) {
                $lastId = $q->lastId();
                if ($lastId !== false) {
                    return response()->redirect("/tasks/$lastId");
                }
            }
            return response()->error(500);
        }
    }
    
    /**
     * PUT /tasks/{id}
     */
    public function update(IRequest $request, $id) {
        
        $task = $request->only([
            'title',
            'user_name',
            'user_email',
            'body',
            'completed',
        ]);
        
        $imagePath = $this->transformImage($request->input('__transform'));
        if ($imagePath !== null) {
            $task['image'] = $imagePath;
        }
        
        /*
         * FIXME: It had to be done by the domain/model classes
         */
        if (! array_key_exists('completed', $task)) {
            $task['completed'] = false;
            
        } else {
            if ($task['completed'] == 'on') {
                $task['completed'] = true;
            }
        }
        
        $validator = new Validator();
        $validator->string('title');
        $validator->string('body');
        $validator->string('user_name');
        $validator->email('user_email');
        $validator->boolean('completed');
        
        if ($validator->fails($task)) {
            
            session()->flash('__input', $request->all());
            session()->flash('__errors', $validator->messages());
            
            return response()->redirect("/tasks/$id/edit");
            
        } else {
            
            $q = Query::make('tasks');
            $status = $q->update($id, $task);
            
            if ($status === true) {
                return response()->redirect("/tasks/$id");
            }
            return response()->error(500);
        }
    }
    
    /**
     * DELETE /tasks/{id}
     */
    public function destroy() {
        return response()->error(501);
    }
}

?>