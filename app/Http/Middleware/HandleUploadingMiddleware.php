<?php

namespace App\Http\Middleware;

use Base\Utility\Files;
use Base\Contracts\Http\IRequest;
use Intervention\Image\ImageManager;
use Base\Contracts\Kernel\IApplication;
use Base\Contracts\Http\ISessionManager;
use Base\Http\Middleware\MiddlewareAdapter;

class HandleUploadingMiddleware extends MiddlewareAdapter {

    /**
     * @Inject
     * @var IApplication
     */
    private $app;

    /**
     * @Inject
     * @var ISessionManager
     */
    private $sessionManager;
    private $imageManager;
    private $uploadDir;
    
    /**
     * using this instead of __construct() magic function because of
     * DI container injects objects after its constructors call
     */
    private function initialize() {

        $this->imageManager = new ImageManager();
        $this->uploadDir = Files::join($this->app->rootPath(), 'public', 'upload');
    }
    
    private function keepUploadedFile($name) {
        
        $file = $_FILES[$name];
        
        if ($file['error'] === UPLOAD_ERR_OK) {
            
            $ext = Files::extension($file['name']);
            $tmpPath = $file['tmp_name'];
            
            $newName = Files::randomFileName($this->uploadDir, ".$ext", 'orig-');
            $destPath = Files::join($this->uploadDir, $newName);
            
            return move_uploaded_file($tmpPath, $destPath) ? $newName : null;
        }
    }

    private function transformImage($uploadName, $transform) {
        
        $ext = Paths::extension($uploadName);
            
        $tx = intval($transform[0]);
        $ty = intval($transform[1]);
        $rw = intval($transform[2]);
        $rh = intval($transform[3]);
        
        $uploadPath = Files::join($this->uploadDir, $uploadName);
        $cropImage = $this->imageManager->make($uploadPath)->resize($rw, $rh)->crop(320, 240, $tx, $ty);
        
        $cropImageName = Files::randomFileName($this->uploadDir, ".$ext", 'cropit-');
        $cropImagePath = Files::join($this->uploadDir, $cropImageName);
        $cropImage->save($cropImagePath);
        
        return "/upload/{$cropImageName}";
    }

    public function handle(IRequest $request, $next) {

        $this->initialize();

        // if ($request->has('__image') && $request->has('__upload_hash')) {

        //     $hash = $request->input('__upload_hash');
        //     $expectedHash = $session->get('user.upload_hash');

        //     if ($hash === $expectedHash) {
                
        //         $uploadName = $this->keepUploadedFile('__image');
        //         if ($uploadName !== null) {
                    
        //             $uploadUri = $this->transformImage($uploadName, $request->input('__transform'));
        //             $request->overwriteInput('__image', $uploadName);
        //         }
        //     }
        // }

        echo HandleUploadingMiddleware::class, '<br>';

        return $next($request);
    }
}