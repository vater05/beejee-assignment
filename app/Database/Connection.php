<?php

namespace App\Database;

use PDO;

/**
 * Database connection singleton
 */
class Connection {
    
    private static $instance;
    
    private function __construct() {}
    private function __clone() {}
    
    public static function instance() {
        
        if (! isset(self::$instance)) {
            self::$instance = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHARSET, DB_USER, DB_PASS);
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$instance;
    }
}

?>