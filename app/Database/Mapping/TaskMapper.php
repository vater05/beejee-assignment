<?php

namespace App\Database\Mapping;

use Base\Contracts\Database\IStatementSource;
use Base\Contracts\Database\Mapping\ITaskMapper;


class TaskMapper extends AbstractMapper implements ITaskMapper {
    
    private $table = 'tasks';
    private $columns = [
        'id',
        'title',
        'body',
        'completed',
    ];
    
    protected function findStatement() {
        
        $columns = join(',', $this->columns);
        return "SELECT $columns FROM $this->table WHERE id = :id";
    }
    
    protected function doLoad(array $data) {
        
        $title = $data['title'];
        $body = $data['body'];
        $isCompleted = boolval($data['completed']);
        
        return new \App\Domain\Task($title, $body, $isCompleted);
    }
    
    public function find($id) {
        return parent::abstractFind($id);
    }
}