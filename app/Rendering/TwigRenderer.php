<?php

namespace App\Rendering;

use App\Contracts\Services\IFileSystem;
use Base\Contracts\Rendering\IRenderer;
use App\Rendering\TwigExtensions\HttpExtension;
use App\Rendering\TwigExtensions\ViewsExtension;

class TwigRenderer implements IRenderer {

    private $loader;
    private $twig;

    public function __construct(IFileSystem $fs) {

        $this->loader = new \Twig_Loader_Filesystem($fs->join($fs->resourcesPath(), 'views'));
        $env = [
            'debug' => TWIG_DEBUG,
            'auto_reload' => TWIG_AUTO_RELOAD,
        ];
        if (TWIG_USE_CACHE === true) {
            $env['cache'] = $fs->join($fs->cachePath(), 'views');
        }
        $this->twig = new \Twig_Environment($this->loader, $env);
        $this->twig->addExtension(new HttpExtension());
        $this->twig->addExtension(new ViewsExtension());
    }

    public function render($view, $args = []) {

        $template = $this->twig->load($view);
        return $template->render($args);
    }
}