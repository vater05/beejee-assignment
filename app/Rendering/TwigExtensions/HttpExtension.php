<?php

namespace App\Rendering\TwigExtensions;

class HttpExtension extends \Twig_Extension {
    
    public function queryStringFunction($extraArgs = []) {
        
        if (is_array($extraArgs)) {
            
            $request = di()->get('App\Contracts\Http\IRequest');
            $query = array_merge($request->query(), $extraArgs);
            
            return http_build_query($query);
        }
        return '';
    }
    
    public function getFunctions() {
        return [
            'query_string' => new \Twig_Function_Method($this, 'queryStringFunction'),
        ];
    }
}