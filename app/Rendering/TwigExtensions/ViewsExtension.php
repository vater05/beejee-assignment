<?php

namespace App\Rendering\TwigExtensions;

class ViewsExtension extends \Twig_Extension {
    
    public function breadcrumbFunction($basePath, $page, $pageCount) {
        
        $numbers = [];
        
        if ($pageCount > 1) {
  
            $leftDots = $page - 3 >= 2;
            $rightDots = $pageCount - $page - 2 >= 2;
            
            if ($leftDots && $rightDots) {
                $numbers = [1, '..', $page - 1, $page, $page + 1, '..', $pageCount];
            
            } else if ($leftDots) {
                $numbers = array_merge([1, '..', $page - 1], range($page, $pageCount));
            
            } else if ($rightDots) {
                $numbers = array_merge(range(1, $page), [$page + 1, '..', $pageCount]);
            
            } else {
                $numbers = range(1, $pageCount);
            }
        }
        
        $args = [
            'page' => $page,
            'page_count' => $pageCount,
            'base_path' => $basePath,
            'numbers' => $numbers,
        ];
        $template = renderer()->load('partial/extensions/breadcrumb.html');
        echo $template->render($args);
    }
    
    public function getFunctions() {
        return [
            'breadcrumb' => new \Twig_Function_Method($this, 'breadcrumbFunction'),
        ];
    }
}