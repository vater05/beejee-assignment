<?php

namespace App\Services;

use App\Contracts\Services\IImageService;
use App\Contracts\Services\IFileSystem;

class ImageService implements IImageService {

    private $fileSystem;

    public function __construct(IFileSystem $fs) {
        $this->fileSystem = $fs;
    }

    public function transformImage(string $imageFile, array $transform) {

        // alias the field
        $fs = $this->fileSystem;

        $uplName = $fs->keepUploadedFile('__image');
        $ext = $fs->ext($uplName);
        
        if ($uplName !== null) {
            
            $tx = intval($transform[0]);
            $ty = intval($transform[1]);
            $rw = intval($transform[2]);
            $rh = intval($transform[3]);
            
            $uplPath = $fs->join($fs->uploadPath(), $uplName);
            $cropImage = image_manager()->make($uplPath)->resize($rw, $rh)->crop(320, 240, $tx, $ty);
            
            $cropImageName = $fs->randomFileName($fs->uploadPath(), ".$ext", 'cropit-');
            $cropImagePath = $fs->join($fs->uploadPath(), $cropImageName);
            $cropImage->save($cropImagePath);
            
            return $fs->uploadUrl($cropImageName);
        }
        return null;
    }
}