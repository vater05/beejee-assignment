<?php

namespace App\Contracts\Database\Mapping;

interface ITaskMapper {
    
    /**
     * @return App\Domain\Task
     */
    public function find($id);
    
    /**
     * @param $source App\Contracts\Database\IStatementSource
     * @return array an array of App\Domain\Task
     */
    public function findMany($source);
}