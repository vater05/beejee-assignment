<?php

namespace App\Contracts\Services;

interface IImageService {

    /**
     * transforms an image
     */
    public function transformImage(string $imageFile, array $transform);
}