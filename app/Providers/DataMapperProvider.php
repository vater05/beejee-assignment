<?php

namespace App\Providers;

use Base\Contracts\Kernel\IProvider;
use App\Database\Mapping\TaskMapper;
use Base\Contracts\Kernel\IApplication;
use App\Contracts\Database\Mapping\ITaskMapper;

class DataMapperProvider implements IProvider {
    
    public function register(IApplication $app) {

        $app->getContainer()->register(
            ITaskMapper::class,
            \DI\object(TaskMapper::class)->constructor()
        );
    }

    public function bootstrap(IApplication $app) {
        // 
    }
}