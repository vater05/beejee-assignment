<?php

namespace App\Domain;

class Task extends DomainObject {
    
    private $title;
    private $body;
    private $isCompleted;
    
    public function __construct($title = '', $body = '', $isCompleted = false) {
        $this->title = $title;
        $this->body = $body;
        $this->isCompleted = $isCompleted;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getBody() {
        return $this->body;
    }
    
    public function isCompleted() {
        return $this->isCompleted;
    }
    
    public function setTitle($title) {
        $this->title = $title;
    }
    
    public function setBody($body) {
        $this->body = $body;
    }
    
    public function setIsCompleted($isCompleted) {
        $this->isCompleted = $isCompleted;
    }
}

?>