<?php

/**
 * -----------------------------------------------------------------------------
 * Simple MVC Web application
 * -----------------------------------------------------------------------------
 * 
 * @author Yan Filippovsky
 */

/*
 * set a constant that holds the project's folder path, like "/var/www/"
 */
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);

if (file_exists(ROOT . 'vendor/autoload.php')) {
    require_once(ROOT . 'vendor/autoload.php');
}

$app = new App\Application(ROOT);
$app->run();

?>